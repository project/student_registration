<?php

namespace Drupal\student_registration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Database\Connection;

/**
 * Provides a Form for Display the result from the db.
 *
 * @package Drupal\student_registration\Controller
 */
class Display extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Showdata.
   *
   * @return string
   *   Return Table format data.
   */

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;


  /**
   * Request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, Connection $connection) {
    $this->requestStack = $request_stack;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database')

    );
  }

  /**
   * Function to show data on page display.
   */
  public function showdata() {

    // You can write your own query to fetch the data I have given my example.
    $result = $this->connection->select('student_form', 'n')
      ->fields('n', ['roll_no',
        'email',
        'fname',
        'lname',
        'gender',
        'age',
        'dateOfAdmission',
        'interests',
        'additonalNotes',
      ])
      ->execute()->fetchAllAssoc('roll_no');
    // Create the row element.
    // $rows = [];.
    foreach ($result as $content) {
      $rows[] = [
        'data' => [
          $content->roll_no,
          $content->email,
          $content->fname,
          $content->lname,
          $content->gender,
          $content->age,
          $content->dateOfAdmission,
          $content->interests,
          $content->additonalNotes,
        ],
      ];
    }
    // Create the header.
    $header = [
      'Roll No.',
      'Email',
      'First Name',
      'Last Name', 'Gender',
      'Age',
      'Date Of Admission',
      'Interests',
      'Additional Notes',
    ];
    // $output = array(
    // '#theme' => 'table',
    // Here you can write #type also instead of #theme.
    // '#header' => $header,
    // '#rows' => $rows
    // );
    // return $output;
    // }
    $email = $this->requestStack->getCurrentRequest()->query->get('email');
    $roll_no = $this->requestStack->getCurrentRequest()->query->get('roll_no');
    $startDate = $this->requestStack->getCurrentRequest()->query->get('startDate');
    $endDate = $this->requestStack->getCurrentRequest()->query->get('endDate');

    $form['form'] = $this->formBuilder()->getForm('Drupal\student_registration\Form\StudentfilterForm');
    $form['form']['filters']['email']['#value'] = $email;
    $form['form']['filters']['roll_no']['#value'] = $roll_no;
    $form['form']['filters']['Date']['startDate']['#value'] = $startDate;
    $form['form']['filters']['Date']['endDate']['#value'] = $endDate;

    // Create table header.
    // $header = [
    // 'id' => $this->t('Id'),
    // 'fname' => $this->t('First Name'),
    // 'sname' => $this->t('Second Name'),
    // 'age'=> $this->t('age'),
    // 'Marks'=> $this->t('Marks'),
    // 'opt' =>$this->t('Operations')
    // ];.
    // $form['student'] = [
    // '#title' => $this->t('Add'),
    // '#type' => 'link',
    // '#url' => Url::fromRoute('dn_students.add_student'),
    // ];.
    $form['show'] = [
      '#title' => $this->t('List'),
      '#type' => 'link',
      '#url' => Url::fromRoute('student_registration.showData'),
    ];

    if ($email == "" && $roll_no == "") {
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => student_registration_get_students("All", "", "", "", ""),
        '#empty' => $this->t('No users found'),
      ];
    }
    else {
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => student_registration_get_students("", $email, $roll_no, strtotime($startDate), strtotime($endDate)),
        '#empty' => $this->t('No records found'),
      ];
    }
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

}
