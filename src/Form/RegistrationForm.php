<?php

namespace Drupal\student_registration\Form;

use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the form for student registration.
 */
class RegistrationForm extends FormBase {
  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Messenger.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * Messenger service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * Account Service.
   *
   * @var account\Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection,
  MessengerInterface $messenger,
  LoggerChannelFactory $logger_factory,
  AccountProxy $account,
  RequestStack $request,
  EntityTypeManagerInterface $entity_type_manager) {
    $this->connection = $connection;
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
    $this->account = $account;
    $this->request = $request;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Inititalizing the constructor.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('messenger'),
      $container->get('logger.factory'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'student_registration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $email = '';
    $field_age = '';
    $field_dateofadmission = '';
    $field_first_name = '';
    $field_last_name = '';
    $field_gender = '';
    $field_rollno = '';
    $field_interests = '';

    if (!$this->account->isAnonymous()) {
      // $uid = $this->request->getCurrentRequest()->get('id');
      $user = $this->entityTypeManager->getStorage('user')->load($this->account->id());
      // $user = User::load($this->account->id());
      $email = $user->get('mail')->value;
      $field_age = $user->get('field_age')->value;
      $field_dateofadmission = $user->get('field_date')->value;
      $field_first_name = $user->get('field_first_name')->value;
      $field_last_name = $user->get('field_last_name')->value;
      $field_gender = $user->get('field_gender')->value;
      $field_rollno = $user->get('field_rollno')->value;
      $field_interests = $user->get('field_interests')->value;
    }
    // dpm($field_dateofadmission);
    $form['student_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Enter Email ID:'),
      '#required' => TRUE,
      '#default_value' => $email,
    ];
    $form['student_Firstname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter First Name:'),
      '#required' => TRUE,
      '#default_value' => $field_first_name,
    ];
    $form['student_Lastname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Last Name:'),
      '#required' => TRUE,
      '#default_value' => $field_last_name,
    ];
    $form['student_gender'] = [
      '#type' => 'select',
      '#title' => ('Select Gender:'),
      '#options' => [
        'Male' => $this->t('Male'),
        'Female' => $this->t('Female'),
      ],
      '#default_value' => $field_gender,
      '#required' => TRUE,
    ];
    $form['student_age'] = [
      '#type' => 'number',
      '#title' => $this->t('Enter Age:'),
      '#required' => TRUE,
      '#default_value' => $field_age,
    ];
    $form['student_rollno'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Roll Number:'),
      '#required' => TRUE,
      '#default_value' => $field_rollno,
    ];
    $form['student_doa'] = [
      '#type' => 'date',
      '#title' => $this->t('Enter Date Of Admission:'),
      '#required' => TRUE,
      '#default_value' => $field_dateofadmission,
    ];
    $form['student_interests'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Select Intersets :'),
      '#required' => TRUE,
      '#options' => [
        'Cricket' => $this->t('Cricket'),
        'Football' => $this->t('Football'),
        'Dancing' => $this->t('Dancing'),
        'Singing' => $this->t('Singing'),
        'Painting' => $this->t('Painting'),
      ],
      '#default_value' => explode(",", $field_interests),
    ];
    $form['student_additionalNotes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional notes for the college authorities:'),
      '#required' => FALSE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => ($email == '' ? $this->t('Register') : $this->t('Update')),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * Function to validate the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->account->isAnonymous()) {
      $roll_no = $form_state->getValue('student_rollno');
      $email = $form_state->getValue('student_mail');
      $sql = $this->connection->select('student_form', 'n')
        ->fields('n', ['email'])->condition('n.email', $email, '=')
        ->execute()->fetchField();
      $sqlRoll_No = $this->connection->select('student_form', 'n')
        ->fields('n', ['email'])->condition('n.roll_no', $roll_no, '=')
        ->execute()->fetchField();
      // dpm($sql);
      if ($sql != FALSE) {
        $form_state->setErrorByName('student_mail', $this->t('This email address already exist'));
      }
      if ($sqlRoll_No != FALSE) {
        $form_state->setErrorByName('student_mail', $this->t('This Roll Number already exist'));
      }
      $age = $form_state->getValue('student_age');
      $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
      if ($age < 0 || $age > 100) {
        $form_state->setErrorByName('student_age', $this->t('Please Enter the valid age'));
      }
      if (strlen($roll_no) > 0 && !preg_match('/^[0-9]{1,10}$/', $roll_no)) {
        $form_state->setErrorByName('student_rollno', $this->t('Please enter a valid roll Number'));
      }
      if (!preg_match($regex, $email)) {
        $form_state->setErrorByName('student_mail', $this->t('The email address is not valid,Please enter vaild email'));
      }
    }
  }

  /**
   * Function used to submit the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // \Drupal::messenger()->addMessage(
    // $this->t("Thank you @firstName @lastName", array('@firstName' =>
    // $form_state->getValue('student_FirstName'),
    // '@lastName' => $form_state->getValue('student_LastName'))));
    // array('@input' => '%input_digits')
    // foreach ($form_state->getValues() as $key => $value) {
    // \Drupal::messenger()->addMessage($key . ': ' . $value);
    // }
    try {
      $conn = Database::getConnection();
      $field = $form_state->getValues();

      $fields["email"] = $field['student_mail'];
      $fields["fname"] = $field['student_Firstname'];
      $fields["lname"] = $field['student_Lastname'];
      $fields["gender"] = $field['student_gender'];
      $fields["age"] = $field['student_age'];
      $fields["roll_no"] = $field['student_rollno'];
      $fields["dateOfAdmission"] = strtotime($field['student_doa']);
      $fields["interests"] = implode(",", $field['student_interests']);
      $fields["additonalNotes"] = $field['student_additionalNotes'];

      if (!$this->account->isAnonymous()) {
        // $re_url = url('<front>');
        $user = $this->entityTypeManager->getStorage('user')->load($this->account->id());
        $field_rollno = $user->get('field_rollno')->value;
        $conn->update('student_form')
          ->fields($fields)->condition('roll_no', $field_rollno)->execute();

        $this->messenger()->addMessage(
            $this->t("Thank you %firstName %lastName your details are updated",
            [
              '%firstName' => $form_state->getValue('student_Firstname'),
              '%lastName' => $form_state->getValue('student_Lastname'),
            ]));
        // $form_state->setRedirectUrl($re_url);
      }
      else {

        $conn->insert('student_form')
          ->fields($fields)->execute();
        $this->messenger()->addMessage(
          $this->t("Thank you %firstName %lastName your details are registered",
          [
            '%firstName' => $form_state->getValue('student_Firstname'),
            '%lastName' => $form_state->getValue('student_Lastname'),
          ]));
        // Not able to use translate funtion while concatenating the strings.
        $user = User::create();
        $user->enforceIsNew();
        $user->activate();
        $user->addRole('student');

        // Mandatory settings.
      }
      $user->setPassword($field['student_mail']);
      $user->setEmail($field['student_mail']);
      $user->setUsername($field['student_Firstname'] . "_" . $field['student_Lastname']);
      $user->set('field_age', $field['student_age']);
      $user->set('field_date', $field['student_doa']);
      $user->set('field_first_name', $field['student_Firstname']);
      $user->set('field_last_name', $field['student_Lastname']);
      $user->set('field_gender', $field['student_gender']);
      $user->set('field_rollno', $field['student_rollno']);
      $user->set('field_interests', implode(",", $field['student_interests']));
      $user->set('field_interests_list', explode(",", implode(",", $field['student_interests'])));
      // Save user.
      // $res =.
      // dpm($field['student_interests'])
      $user->save();

      user_login_finalize($user);
      // dpm($res);
    }
    catch (Exception $ex) {
      $this->loggerFactory->get('student_registration')->error($ex->getMessage());
    }
  }

}
