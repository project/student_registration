<?php

namespace Drupal\student_registration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the form for filter Students.
 */
class StudentfilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'student_registration_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['filters'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Filter'),
      '#open'  => TRUE,
    ];

    $form['filters']['email'] = [
      '#title'         => 'Email',
      '#type'          => 'search',
    ];
    $form['filters']['roll_no'] = [
      '#title'         => 'Roll No',
      '#type'          => 'search',
    ];
    $form['filters']['Date'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Date Range'),
      '#open'  => TRUE,
    ];
    $form['filters']['Date']['startDate'] = [
      '#title'         => 'Start Date',
      '#type'          => 'date',
    ];
    $form['filters']['Date']['endDate'] = [
      '#title'         => 'End Date',
      '#type'          => 'date',
    ];
    $form['filters']['actions'] = [
      '#type'       => 'actions',
    ];

    $form['filters']['actions']['submit'] = [
      '#type'  => 'submit',
      '#name' => 'filter',
      '#value' => $this->t('Filter'),

    ];
    $form['filters']['actions']['reset'] = [
      '#type'  => 'submit',
      '#name'  => 'reset',
      '#value' => $this->t('Reset'),

    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // If ( $form_state->getValue('startDate') != "" ) {
    // $form_state->setErrorByName('from',
    // $this->t('You must enter a valid first name.'));
    // } elseif( $form_state->getValue('marks') == ""){
    // $form_state->setErrorByName('marks',
    // $this->t('You must enter a valid to marks.'));
    // }.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $field = $form_state->getValues();
    $email = $field["email"];
    $roll_no = $field["roll_no"];
    $startDate = $field["startDate"];
    $endDate = $field["endDate"];

    $button_clicked = $form_state->getTriggeringElement()['#name'];
    if ($button_clicked == 'filter') {
      $url = Url::fromRoute('student_registration.showData')
        ->setRouteParameters(
          [
            'email' => $email,
            'roll_no' => $roll_no,
            'startDate' => $startDate,
            'endDate' => $endDate,
          ]);
      $form_state->setRedirectUrl($url);
    }
    else {
      $form_state->setRedirect('student_registration.showData');
    }
  }

}
