<?php

namespace Drupal\student_registration\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides a 'Registration' Block.
 *
 * @Block(
 *   id = "registration_block",
 *   admin_label = @Translation("Registration Block"),
 *   category = @Translation("Registration Block"),
 * )
 */
class RegistrationBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Account Service.
   *
   * @var account\Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Registration block object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Drupal\Core\Session\AccountProxy $account
   *   Account of current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   User Entity.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, AccountProxy $account, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->account = $account;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiate this block class.
    return new static($configuration, $plugin_id, $plugin_definition,
      // Load the service required to construct this class.
      $container->get('form_builder'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('Drupal\student_registration\Form\RegistrationForm');
    if ($this->account->isAnonymous()) {
      return $form;
    }
    else {
      $user = $this->entityTypeManager->getStorage('user')->load($this->account->id());
      $email = $user->get('mail')->value;
      $name = $user->get('name')->value;
      $field_age = $user->get('field_age')->value;
      $field_dateofadmission = $user->get('field_date')->value;
      $field_first_name = $user->get('field_first_name')->value;
      $field_last_name = $user->get('field_last_name')->value;
      $field_gender = $user->get('field_gender')->value;
      $field_rollno = $user->get('field_rollno')->value;
      $field_interests = $user->get('field_interests')->value;

      return [
        '#theme' => 'students_add_form',
        "#email" => $email,
        "#name" => $name,
        "#field_age" => $field_age,
        "#field_dateofadmission" => $field_dateofadmission,
        "#field_first_name" => $field_first_name,
        "#field_last_name" => $field_last_name,
        "#field_gender" => $field_gender,
        "#field_rollno" => $field_rollno,
        "#field_interests" => $field_interests,
      ];
    }
    // Or use this method
    // return [
    // '#theme' => 'mytheme',
    // "#form" => $form,
    // "#data" => $data,
    // ];
    //
    // in mytheme.html.twig use {{ form }} and {{ data }}.
  }

}
